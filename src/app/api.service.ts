import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { environment as ENV } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private headers = new HttpHeaders({
    // 'Content-Type': 'application/json',
  });
  private options = {
    headers: this.headers,
  }

  constructor(private http: HttpClient) { }

  get httpOptionsAuth() {
    return {
      headers: new HttpHeaders({
      })
    };
  }

  // post(url: string, data: any): Observable<any> {
  //   return this.http.post<any>(`${ENV.api_url + url}`, data, this.httpOptionsAuth)
  //     .pipe(
  //       map((res) => {
  //         return res;
  //       }));
  // }

  // get(url: string): Observable<any> {
  //   return this.http.get<any>(`${ENV.api_url + url}`, this.httpOptionsAuth)
  //     .pipe(
  //       map((res) => {
  //         return res;
  //       }));
  // }


}
