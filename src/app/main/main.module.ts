import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HelpComponent } from './help/help.component';

@NgModule({
  declarations: [
    DashboardComponent,
    HelpComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    NgbModule
  ]
})
export class MainModule { }
