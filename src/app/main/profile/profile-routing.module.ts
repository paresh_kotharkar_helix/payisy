import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileMainComponent } from './profile-main/profile-main.component';
import { ProfileResetConfirmComponent } from './profile-reset-confirm/profile-reset-confirm.component';
import { ProfileResetPasswordComponent } from './profile-reset-password/profile-reset-password.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileMainComponent
  },
  {
    path: 'profile-reset',
    component: ProfileResetPasswordComponent
  },
  {
    path: 'profile-reset-confirm',
    component: ProfileResetConfirmComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
