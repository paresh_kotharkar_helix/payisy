import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileMainComponent } from './profile-main/profile-main.component';
import { ProfileResetConfirmComponent } from './profile-reset-confirm/profile-reset-confirm.component';
import { ProfileResetPasswordComponent } from './profile-reset-password/profile-reset-password.component';

@NgModule({
  declarations: [
    ProfileMainComponent,
    ProfileResetConfirmComponent,
    ProfileResetPasswordComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    NgbModule
  ]
})
export class ProfileModule { }
