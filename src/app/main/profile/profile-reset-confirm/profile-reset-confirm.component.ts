import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-reset-confirm',
  templateUrl: './profile-reset-confirm.component.html',
  styleUrls: ['./profile-reset-confirm.component.css']
})
export class ProfileResetConfirmComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
