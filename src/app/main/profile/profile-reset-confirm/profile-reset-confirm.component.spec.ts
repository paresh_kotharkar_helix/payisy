import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileResetConfirmComponent } from './profile-reset-confirm.component';

describe('ProfileResetConfirmComponent', () => {
  let component: ProfileResetConfirmComponent;
  let fixture: ComponentFixture<ProfileResetConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileResetConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileResetConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
