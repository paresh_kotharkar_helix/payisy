import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HelpComponent } from './help/help.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
  {
    path: 'help',
    component: HelpComponent
  },
  {
    path: "advance-withdrawal",
    loadChildren: () => import(`./salary-advance/salary-advance.module`).then(m => m.SalaryAdvanceModule),
  },
  {
    path: "transaction",
    loadChildren: () => import(`./transaction/transaction.module`).then(m => m.TransactionModule),
  },
  {
    path: "profile",
    loadChildren: () => import(`./profile/profile.module`).then(m => m.ProfileModule),
  },
  {
    path: "notifications",
    loadChildren: () => import(`./notification/notification.module`).then(m => m.NotificationModule),
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
