import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationRoutingModule } from './notification-routing.module';
import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationDetailsComponent } from './notification-details/notification-details.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    NotificationsComponent,
    NotificationDetailsComponent
  ],
  imports: [
    CommonModule,
    NotificationRoutingModule,
    SharedModule,
    NgbModule
  ]
})
export class NotificationModule { }
