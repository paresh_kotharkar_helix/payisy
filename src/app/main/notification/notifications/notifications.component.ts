import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor(private location: Location,
    private router: Router,
    private elementRef: ElementRef) { }

  ngOnInit(): void { }
  back() {
    this.location.back();
  }
  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument
      .body.style.backgroundColor = '#e9ecef';
  }
  ngOnDestroy() {
    this.elementRef.nativeElement.ownerDocument
      .body.style.backgroundColor = '#ffff';
  }
}
