import { AfterViewInit, Component, ElementRef, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbCarouselConfig, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.css']
})
export class TransactionHistoryComponent implements OnInit, AfterViewInit, OnDestroy {
  viewType: string = 'history';
  private document: any;
  constructor(private location: Location,
    config: NgbCarouselConfig,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private elementRef: ElementRef) { }

  ngOnInit(): void {

  }
  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument
      .body.style.backgroundColor = '#e9ecef';
  }
  ngOnDestroy() {
    this.elementRef.nativeElement.ownerDocument
      .body.style.backgroundColor = '#ffff';
  }
  back() {
    this.location.back();
  }
  changeViewType(type: string) {
    this.viewType = type;
  }

  closeResult = '';
  openModal1(id?: any) {
    this.modalService.open(id, {
      backdrop: 'static',
      centered: true,
    });
  }

  open(content1: any) {
    this.modalService.open(content1).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
