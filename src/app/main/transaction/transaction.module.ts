import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionRoutingModule } from './transaction-routing.module';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { TransactionDetailsComponent } from './transaction-details/transaction-details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    TransactionHistoryComponent,
    TransactionDetailsComponent
  ],
  imports: [
    CommonModule,
    TransactionRoutingModule,
    SharedModule, NgbModule
  ]
})
export class TransactionModule { }
