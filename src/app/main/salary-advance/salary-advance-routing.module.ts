import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdvanceWithdrawalComponent } from './advance-withdrawal/advance-withdrawal.component';
import { WithdrawalConfirmComponent } from './withdrawal-confirm/withdrawal-confirm.component';

const routes: Routes = [
  {
    path: 'advance-withdrawal',
    component: AdvanceWithdrawalComponent
  },
  {
    path: 'withdrawal-confirm',
    component: WithdrawalConfirmComponent
  },
  {
    path: '',
    component: AdvanceWithdrawalComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalaryAdvanceRoutingModule { }
