import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalaryAdvanceRoutingModule } from './salary-advance-routing.module';
import { AdvanceWithdrawalComponent } from './advance-withdrawal/advance-withdrawal.component';
import { WithdrawalConfirmComponent } from './withdrawal-confirm/withdrawal-confirm.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    AdvanceWithdrawalComponent,
    WithdrawalConfirmComponent
  ],
  imports: [
    CommonModule,
    SalaryAdvanceRoutingModule,
    SharedModule, NgbModule
  ]
})
export class SalaryAdvanceModule { }
