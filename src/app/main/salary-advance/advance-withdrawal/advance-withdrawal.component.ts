import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-advance-withdrawal',
  templateUrl: './advance-withdrawal.component.html',
  styleUrls: ['./advance-withdrawal.component.css']
})
export class AdvanceWithdrawalComponent implements OnInit {
  viewType: string = 'advsalary';
  advsalary = true;
  constructor(private location: Location) { }

  ngOnInit(): void { }
  back() {
    this.location.back();
  }
  changeViewType(type: string) {
    this.viewType = type;
  }


}
