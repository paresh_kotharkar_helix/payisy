import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvanceWithdrawalComponent } from './advance-withdrawal.component';

describe('AdvanceWithdrawalComponent', () => {
  let component: AdvanceWithdrawalComponent;
  let fixture: ComponentFixture<AdvanceWithdrawalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvanceWithdrawalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
