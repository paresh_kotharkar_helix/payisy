import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-withdrawal-confirm',
  templateUrl: './withdrawal-confirm.component.html',
  styleUrls: ['./withdrawal-confirm.component.css']
})
export class WithdrawalConfirmComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit(): void { }
  back() {
    this.location.back();
  }

}
