import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { SplashComponent } from './splash/splash.component';
import { OtpComponent } from './registration/otp/otp.component';
import { RegistrationProfileComponent } from './registration/registration-profile/registration-profile.component';
import { RegistrationConfirmationComponent } from './registration/registration-confirmation/registration-confirmation.component';
import { LoginComponent } from './login/login.component';
import { LoginConfirmationComponent } from './login/login-confirmation/login-confirmation.component';
import { ResetComponent } from './login/reset/reset.component';
import { SetPinComponent } from './login/set-pin/set-pin.component';
import { PinSuccessComponent } from './login/pin-success/pin-success.component';
import { ForgotPinComponent } from './login/forgot-pin/forgot-pin.component';
import { NewPinComponent } from './login/new-pin/new-pin.component';
import { ResetSuccessPinComponent } from './login/reset-success-pin/reset-success-pin.component';
import { ResetPasswordComponent } from './login/reset-password/reset-password.component';
import { NewPasswordComponent } from './login/new-password/new-password.component';
import { SuccessPasswordComponent } from './login/success-password/success-password.component';

const routes: Routes = [
  {
    path: '',
    component: SplashComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'otp',
    component: OtpComponent
  },
  {
    path: 'registration-profile',
    component: RegistrationProfileComponent
  },
  {
    path: 'registration-confirm',
    component: RegistrationConfirmationComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'login-otp',
    component: LoginConfirmationComponent
  },
  {
    path: 'reset',
    component: ResetComponent
  },
  {
    path: 'set-pin',
    component: SetPinComponent
  },
  {
    path: 'pin-success',
    component: PinSuccessComponent
  },
  {
    path: 'forgot-pin',
    component: ForgotPinComponent
  },
  {
    path: 'new-pin',
    component: NewPinComponent
  },
  {
    path: 'reset-success-pin',
    component: ResetSuccessPinComponent
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'new-password',
    component: NewPasswordComponent
  },
  {
    path: 'success-password',
    component: SuccessPasswordComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardingRoutingModule { }
