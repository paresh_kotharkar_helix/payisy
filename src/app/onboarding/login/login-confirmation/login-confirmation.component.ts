import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login-confirmation',
  templateUrl: './login-confirmation.component.html',
  styleUrls: ['./login-confirmation.component.css']
})
export class LoginConfirmationComponent implements OnInit {
 
 constructor(private location: Location) { }

  ngOnInit(): void { }
  back() {
    this.location.back();
  }


}
