import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-success-password',
  templateUrl: './success-password.component.html',
  styleUrls: ['./success-password.component.css']
})
export class SuccessPasswordComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit(): void { }
  back() {
    this.location.back();
  }

}
