import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pin-success',
  templateUrl: './pin-success.component.html',
  styleUrls: ['./pin-success.component.css']
})
export class PinSuccessComponent implements OnInit {

    constructor(private location: Location) { }

    ngOnInit(): void { }
    back() {
      this.location.back();
    }

}
