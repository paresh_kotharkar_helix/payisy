import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PinSuccessComponent } from './pin-success.component';

describe('PinSuccessComponent', () => {
  let component: PinSuccessComponent;
  let fixture: ComponentFixture<PinSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PinSuccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PinSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
