import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-reset-success-pin',
  templateUrl: './reset-success-pin.component.html',
  styleUrls: ['./reset-success-pin.component.css']
})
export class ResetSuccessPinComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit(): void { }
  back() {
    this.location.back();
  }


}
