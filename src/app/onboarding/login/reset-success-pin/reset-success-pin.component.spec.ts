import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetSuccessPinComponent } from './reset-success-pin.component';

describe('ResetSuccessPinComponent', () => {
  let component: ResetSuccessPinComponent;
  let fixture: ComponentFixture<ResetSuccessPinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResetSuccessPinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetSuccessPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
