import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-pin',
  templateUrl: './new-pin.component.html',
  styleUrls: ['./new-pin.component.css']
})
export class NewPinComponent implements OnInit {

  password1: any;
  password2: any;
  show1 = false;
  show2 = false;

  form: FormGroup = this.fb.group({
    newpin: ['', [Validators.required, Validators.maxLength(30)]],
    confirmpin: ['', [Validators.required, Validators.maxLength(30)]],
  })
  submitted: boolean = false;

  constructor(private fb: FormBuilder, private router: Router, private location: Location) { }

  ngOnInit(): void {

  }
  resetPin() {
    console.log("form", this.form);
    this.submitted = true;
    if (this.form.valid) {
      let data = {
        'newpin': this.f.newpin.value,
        'confirmpin': this.f.confirmpin.value,
      }
      console.log("data", data);
      this.router.navigate(['/reset-success-pin']);
    }
  }

  get f() {
    return this.form.controls;
  }
  onClear() {
    this.form.reset();
  }
  back() {
    this.location.back();
  }

  onClick() {
    if (this.password1 === 'password') {
      this.password1 = 'text';
      this.show1 = true;
    } else {
      this.password1 = 'password';
      this.show1 = false;
    }
  }
  onClick1() {
    if (this.password2 === 'password') {
      this.password2 = 'text';
      this.show2 = true;
    } else {
      this.password2 = 'password';
      this.show2 = false;
    }
  }
}
