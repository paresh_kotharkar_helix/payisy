import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  password: any;
  show = false;

  form: FormGroup = this.fb.group({
    username: ['', [Validators.required, Validators.maxLength(30)]],
    password: ['', [Validators.required, Validators.maxLength(30)]]
  })
  submitted: boolean = false;
  constructor(private fb: FormBuilder, private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.password = 'password';
  }

  login() {
    this.submitted = true;
    console.log("form", this.form);
    if (this.form.valid) {
      let data = {
        'username': this.f.username.value,
        'password': this.f.password.value,
      }
      console.log("data", data);
      this.router.navigate(['/login-otp']);
    }
  }
  onClear() {
    this.form.reset();
  }
  get f() {
    return this.form.controls
  }

  back() {
    this.location.back();
  }

  onClick() {
    if (this.password === 'password') {
      this.password = 'text';
      this.show = true;
    } else {
      this.password = 'password';
      this.show = false;
    }
  }
}
