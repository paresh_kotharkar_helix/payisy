import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {

  password1: any;
  password2: any;
  password3: any;
  show1 = false;
  show2 = false;
  show3 = false;

  form: FormGroup = this.fb.group({
    oldpassword: ['', [Validators.required, Validators.maxLength(30)]],
    newpassword: ['', [Validators.required, Validators.maxLength(30)]],
    confirmpassword: ['', [Validators.required, Validators.maxLength(30)]]
  })
  submitted: boolean = false;

  constructor(private fb: FormBuilder, private router: Router, private location: Location) { }

  ngOnInit(): void {

  }
  onresetPassword() {
    console.log("form", this.form);
    this.submitted = true;
    if (this.form.valid) {
      let data = {
        'oldpassword': this.f.oldpassword.value,
        'newpassword': this.f.newpassword.value,
        'confirmpassword': this.f.confirmpassword.value,
      }
      console.log("data", data);
      this.router.navigate(['/success-password']);
    }
  }

  get f() {
    return this.form.controls;
  }

  onClear() {
    this.form.reset();
  }

  back() {
    this.location.back();
  }

  onClick() {
    if (this.password1 === 'password') {
      this.password1 = 'text';
      this.show1 = true;
    } else {
      this.password1 = 'password';
      this.show1 = false;
    }
  }
  onClick1() {
    if (this.password2 === 'password') {
      this.password2 = 'text';
      this.show2 = true;
    } else {
      this.password2 = 'password';
      this.show2 = false;
    }
  }
  onClick2() {
    if (this.password3 === 'password') {
      this.password3 = 'text';
      this.show3 = true;
    } else {
      this.password3 = 'password';
      this.show3 = false;
    }
  }

}
