import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnboardingRoutingModule } from './onboarding-routing.module';
import { SplashComponent } from './splash/splash.component';
import { RegistrationComponent } from './registration/registration.component';
import { OtpComponent } from './registration/otp/otp.component';
import { RegistrationProfileComponent } from './registration/registration-profile/registration-profile.component';
import { RegistrationConfirmationComponent } from './registration/registration-confirmation/registration-confirmation.component';
import { LoginComponent } from './login/login.component';
import { LoginConfirmationComponent } from './login/login-confirmation/login-confirmation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { ResetComponent } from './login/reset/reset.component';
import { SetPinComponent } from './login/set-pin/set-pin.component';
import { PinSuccessComponent } from './login/pin-success/pin-success.component';
import { ForgotPinComponent } from './login/forgot-pin/forgot-pin.component';
import { NewPinComponent } from './login/new-pin/new-pin.component';
import { ResetSuccessPinComponent } from './login/reset-success-pin/reset-success-pin.component';
import { ResetPasswordComponent } from './login/reset-password/reset-password.component';
import { NewPasswordComponent } from './login/new-password/new-password.component';
import { SuccessPasswordComponent } from './login/success-password/success-password.component';

@NgModule({
  declarations: [
    SplashComponent,
    RegistrationComponent,
    OtpComponent,
    RegistrationProfileComponent,
    RegistrationConfirmationComponent,
    LoginComponent,
    LoginConfirmationComponent,
    ResetComponent,
    SetPinComponent,
    PinSuccessComponent,
    ForgotPinComponent,
    NewPinComponent,
    ResetSuccessPinComponent,
    ResetPasswordComponent,
    NewPasswordComponent,
    SuccessPasswordComponent
  ],
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ShowHidePasswordModule
  ]
})
export class OnboardingModule { }
