import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  form: FormGroup = this.fb.group({
    cmpcode: ['', [Validators.required, Validators.maxLength(30)]],
    mobilenumber: ['', [Validators.required, Validators.maxLength(10)]]
  })
  submitted: boolean = false;
  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
  }
  onRegistration() {
    console.log("form", this.form);
    this.submitted = true;
    if (this.form.valid) {
      let data = {
        'cmpcode': this.f.cmpcode.value,
        'mobilenumber': this.f.mobilenumber.value
      }
      console.log("data", data);
      this.router.navigate(['/otp']);
    }
  }

  get f() {
    return this.form.controls;
  }

  onClear() {
    this.form.reset();
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
