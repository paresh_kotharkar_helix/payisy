import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration-profile',
  templateUrl: './registration-profile.component.html',
  styleUrls: ['./registration-profile.component.css']
})
export class RegistrationProfileComponent implements OnInit {
  password: any;
  password1: any;
  password2: any;
  password3: any;

  show = false;
  show1 = false;
  show2 = false;
  show3 = false;

  form: FormGroup = this.fb.group({
    username: ['', [Validators.required, Validators.maxLength(30)]],
    password: ['', [Validators.required, Validators.maxLength(30)]],
    cpassword: ['', [Validators.required, Validators.maxLength(30)]],
    pin: ['', [Validators.required, Validators.maxLength(30)]],
    cpin: ['', [Validators.required, Validators.maxLength(30)]],

  })
  submitted: boolean = false;

  constructor(private fb: FormBuilder, private router: Router, private location: Location) { }

  ngOnInit() {
  }

  registration() {
    console.log("form", this.form);
    this.submitted = true;
    if (this.form.valid) {
      let data = {
        'username': this.f.username.value,
        'password': this.f.password.value,
        'cpassword': this.f.cpassword.value,
        'pin': this.f.pin.value,
        'cpin': this.f.cpin.value,
      }
      console.log("data", data);
      this.router.navigate(['/registration-confirm']);
    }
  }

  get f() {
    return this.form.controls;
  }
  onClear() {
    this.form.reset();
  }
  back() {
    this.location.back();
  }

  onClick() {

    if (this.password === 'password') {
      this.password = 'text';
      this.show = false;
    } else {
      this.password = 'password';
      this.show = true;
    }

  }
  onClick1() {
    if (this.password1 === 'password') {
      this.password1 = 'text';
      this.show1 = false;
    } else {
      this.password1 = 'password';
      this.show1 = true;
    }
  }
  onClick2() {
    if (this.password2 === 'password') {
      this.password2 = 'text';
      this.show2 = false;
    } else {
      this.password2 = 'password';
      this.show2 = true;
    }
  }
  onClick3() {
    if (this.password3 === 'password') {
      this.password3 = 'text';
      this.show3 = false;
    } else {
      this.password3 = 'password';
      this.show3 = true;
    }
  }
}
